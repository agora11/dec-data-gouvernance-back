//Version templatefile maven : 1.6.0

//déclaration de la shared library build_libs
@Library('build_libs') _

//Propriétés spécifiques au job
//Nombre de build a conserver dans l'historique
def NUM_TO_KEEP = 10
//Nom de la connexion à Gitlab
def GITLAB_NAME = 'Gitlab RES cdsdpa-dsir'
//Id du fichier de configuration maven à utiliser
def MAVEN_CONFIG_ID = 'maven-settings-nexusm-repo-res'
//La branche par défaut à partir de laquelle, le déploiement nexus et l'analyse sonar doivent être faites
def BRANCH_DEVELOP = 'develop'
//version de maven utilisé pour le build
def MAVEN_VERSION = '3.5.3-jdk-8'

/** Spécifique pour les builds docker */

def prepareBuildDocker = [
                    
                    
                    // Paramètre optionnel :
                    // Id du credentials Jenkins à utiliser pour que le build docker puisse tirer les images From des dockerfiles lorsque présentes sur une organisation privée de la DTR et non publique.
                    // 'credentialsIdBuild': '{{credentialsIdBuild }}',
                    
                    // Paramètre optionnel :
                    // Nom du dépôt maven du service nexus sur lequel seront poussés les binaires lors de ce build.
                    // Il sera injecté en tant que build-arg "REPO_NAME" lors du build de l'image. Les dockerfiles doivent contenir l'instruction ARG REPO_NAME
                    // 'repoBinaires':'{{ maven-repo }}',
                    
                    // Paramètre optionnel :
                    // id du credential permettant de downloader les binaires présents sur le service Nexus
                    // 'credentialsIdDownload':'{{credentialsId-download}}',
                    
                    // Paramètre optionnel :
                    // Le nom des images est construit à partir du nom du dépôt git. Ce paramètre permet d'utiliser une valeur différente à la place du nom du dépot.
                    // Pour les projets multi-modules, le nom de l'image sera construite à partir de ce préfixe et du nom du module dans les sources sous le dossier docker.
                    // Pour les projets non modulaire, le nom de l'image correspondra à ce préfixe.
                    //'prefixImage': '{{ prefix-Image }}',
                    
                    //Paramètre optionnel :
                    //Permet de passer en argument du build docker une variable no_proxy, qui par défaut est valorisée avec les adresses en .sncf.fr et l'adresse du nexus metier
                    //'no_proxy': '{{ no-proxy}}',
         
                    // Chemin du pom maven local au workspace
                    'pomPath':'pom.xml'
]




//Propriétés du job
properties([[$class: 'BuildConfigProjectProperty'],
    //Connexion Gitlab
    gitLabConnection("${GITLAB_NAME}"),
    //Conservation des 10 dernières éxecutions
    buildDiscarder(logRotator(numToKeepStr: "${NUM_TO_KEEP}")),
    //Déclenchement du job via webhook lors de push de nouveau code ou de creation de merge request
    pipelineTriggers([[$class: 'GitLabPushTrigger', triggerOnPush: true, triggerOnMergeRequest: true, branchFilterType: 'All']])])

//Plugin timestamp pour afficher le temps à chaque ligne de log
timestamps {
   withTools([name: 'maven', version: "${MAVEN_VERSION}"]) {
      try {
          stage('Récupération code source') {
              scmInfo = checkout scm
              env.GIT_URL =  scmInfo.GIT_URL
              env.GIT_COMMIT = scmInfo.GIT_COMMIT

          }
          //Décommenter pour activer le build docker
          
          stage('Preparation build docker'){
              def pom = readMavenPom file: prepareBuildDocker['pomPath']
              prepareBuildDocker['version'] = pom.getVersion()


          }


          //Phase de compilation, publication du nombre de TODOs
          withMaven(mavenSettingsConfig:"${MAVEN_CONFIG_ID}", options: [openTasksPublisher()], publisherStrategy: 'EXPLICIT') {
              container('maven') {
                  stage('Compilation') {
                      sh "mvn $MAVEN_CONFIG clean compile -B -U"
                  }
              }
          }
          //Phase de tests, publication des résultats de TUs
          withMaven(mavenSettingsConfig:"${MAVEN_CONFIG_ID}", publisherStrategy: 'EXPLICIT',options: [junitPublisher(healthScaleFactor: 1.0)]) {
              container('maven') {
                 stage('TUs') {
                      sh "mvn $MAVEN_CONFIG test -B -Dmaven.test.failure.ignore=true"
                  }
              }
          }
          //Phase d'upload, publication sur la page du jobs, des artefacts poussés uniquement sur la branche de developpement par defaut
          if (env.BRANCH_NAME == BRANCH_DEVELOP) {
              withMaven(mavenSettingsConfig:"${MAVEN_CONFIG_ID}", options: [artifactsPublisher()], publisherStrategy: 'EXPLICIT') {
                  container('maven') {
                      stage('Upload binaires') {
                          sh "mvn $MAVEN_CONFIG -B deploy -DskipTests=true"
                      }
                  }
              }
              //phase n'analyse qualité uniquement sur la branche de developpement par defaut
             /* withMaven(mavenSettingsConfig:"${MAVEN_CONFIG_ID}", publisherStrategy: 'EXPLICIT') {
                  container('maven') {
                       stage('analyse SonarQube') {
                          def sonarBranch = env.BRANCH_NAME.replaceAll(/[\\]/,"_").replaceAll(/[, ]/,"")
                          echo "branchesonar $sonarBranch"
                          withSonarQubeEnv("sonarqube") {
                              sh "mvn $MAVEN_CONFIG sonar:sonar -DskipTests=true -Dsonar.branch=$sonarBranch"
                          }
                      }
                  }
              }*/
              //Décommenter pour activer le build docker
            
              if ( currentBuild.result != 'FAILURE'){

              // Réalise les étapes de builds et de push des images des modules du projet
                  buildDocker(prepareBuildDocker)
              }

          }
          //voir https://jenkins.io/doc/pipeline/steps/gitlab-plugin/ pour plus de précisions
          updateGitlabCommitStatus name: 'build', state: 'success'
          currentBuild.result = 'SUCCESS'

      } catch (all) {
          //voir https://jenkins.io/doc/pipeline/steps/gitlab-plugin/ pour plus de précisions
              updateGitlabCommitStatus name: 'build', state: 'failed'
              currentBuild.result = 'FAILURE'
              //Envoi d'un mail en cas d'échec - COMMENTE EN ATTENTE DE L'INSTALLATION DU PLUGIN EMAIL EXT
              //voir https://jenkins.io/doc/pipeline/steps/email-ext/ pour plus de précisions
              emailext(
                  body:'${DEFAULT_CONTENT}',
                  subject:'${DEFAULT_SUBJECT}',
                  //envoie du mail aux développeurs responsables d'une modification du code
                  recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']],
                  //Pour également envoyer le mail à une liste d'adresses (séparées par des ,)
              //  to : "adressmail1, adressmail2"
              )
             throw all
      }
  }
}
