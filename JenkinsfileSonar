#!/usr/bin/env groovy
//Version templatefile maven : 1.6.0

//déclaration de la shared library build_libs
@Library('build_libs') _

//Propriétés spécifiques au job
//Nombre de build a conserver dans l'historique
def NUM_TO_KEEP = 10
//Id du fichier de configuration maven à utiliser
def MAVEN_CONFIG_ID = 'maven-settings-nexusm-repo-res'
//version de maven utilisé pour le build
def MAVEN_VERSION = '3.5.3-jdk-8'


//Propriétés du job
properties([[$class: 'BuildConfigProjectProperty'],
    //Conservation des 10 dernières éxecutions
    buildDiscarder(logRotator(numToKeepStr: "${NUM_TO_KEEP}")),
    //Paramètres du build sonar
    parameters([string(defaultValue: '1.0.0', description: 'Tag de la release sur lequelle lancer l\'analyse', name: 'TAG_RELEASE')
                ])
    ])

//Plugin timestamp pour afficher le temps à chaque ligne de log
timestamps {
   withTools([name: 'maven', version: "${MAVEN_VERSION}"]) {
      try {
          stage('Récupération code source') {
              checkout scm
          }

          //Phase de controle du passage de la quality gate
          withMaven(mavenSettingsConfig: "${MAVEN_CONFIG_ID}", options: [openTasksPublisher()], publisherStrategy: 'EXPLICIT') {
              container('maven') {
                  stage('Analyse SonarQube') {
                      withSonarQubeEnv("sonarqube") {
                          sh "mvn $MAVEN_CONFIG clean org.jacoco:jacoco-maven-plugin:prepare-agent package sonar:sonar"
                      }
                  }
              }
          }

          currentBuild.result = 'SUCCESS'

      } catch (all) {
          currentBuild.result = 'FAILURE'
          //Envoi d'un mail en cas d'échec
          //voir https://jenkins.io/doc/pipeline/steps/email-ext/ pour plus de précisions
          emailext(
              body:'${DEFAULT_CONTENT}',
              subject:'${DEFAULT_SUBJECT}',
              //envoie du mail aux développeurs responsables d'une modification du code
              recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']],
              //Pour également envoyer le mail à une liste d'adresses (séparées par des ,)
          //  to : "adressmail1, adressmail2"
          )

          throw all
      }
  }
}
