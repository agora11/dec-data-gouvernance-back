package sncf.reseau.cdsdpadsir.datagouvernance.web;

public class ServerJs {
	// to update with environnement and message
	public static final String VERSION_ID = "PORTAIL_4.0.8_06_11_19" + "Profils Caiman";

	// A activer pour la prod MEE 1.8 du 26_06_19
	// old : PORTAIL_2.3.1_MEE_04_04_19#jdbc:postgresql://172.26.24.79/pdpodwh|dpodba
	// public static final String VERSION_ID = "PORTAIL_3_4_CHE_03-06-19_PROD";

	// public static final String SERVER_URL = "http://localhost:4200";
	public static final String API = "/";

	// public static final String SERVER_URL = "http://rffsrvdpo50001.rff.ferre"; public static final String API = "/"; // DEV
	// public static final String SERVER_URL = "http://rffsrvdpo30001.rff.ferre"; public static final String API = "/"; // RECETTE
	// public static final String SERVER_URL = "http://rffsrvdpo00001.rff.ferre"; public static final String API = "/"; // PROD
	// public static final String API = "/"; // ALL SAUF LOCALHOST commenter CrossOrigin

	// public static final String SERVER_URL = "http://localhost:4200"; public static final String API = "/portail/back/"; // local emulation recette
	// public static final String SERVER_URL = "http://localhost:4200"; public static final String API = "/"; // local
	// public static final String SERVER_URL = "http://rffsrvdpo50001.rff.ferre"; public static final String API = "/portail/back/"; // DEV
	// public static final String SERVER_URL = "http://rffsrvdpo30001.rff.ferre"; public static final String API = "/portailtest/"; // RECETTE TEST
	// public static final String SERVER_URL = "http://rffsrvdpo30001.rff.ferre"; public static final String API = "/"; // RECETTE CAIMAN et NEW DEV
	// OK
	// GED parametersmvn clean package

	// ARCOLE PARAMETERS
	public static final String NOM_IDENTIFIANT = "passio_cs10";

	// public static final String LOGIN_USER = "7509148t"; //8611797z(login)
	public static final String LOGIN_USER = "PASSIO_AGENT";
	public static final String SECUREMENT_ACTION = "UsernameToken";
	public static final String SECUREMENT_PASSWORD_TYPE = "PasswordText";
	public static final String SECUREMENT_PASSWORD_RECETTE = "i48E+IzcQKQ012/6YS4ARpGk6WiOJeTkntm6hJD3DU/iNY+X0Pf/n6DB1HAQDKnl52YetlkvVFIqagoBUy33SeLIscs3K06Sveprpbucdr0VTpk2mrR1ce+r7v+Vzc/K7lJKC6m/XObiEnwA/26EX60PGBQlS/2+2CNX8C4916K4xb2+K6qvEfLwBa+Mq0bgV8fJ2NdY+liQx8plahJ09MCqKHREfIH5TpfwDLDdB2q09I3lJdsx+qPuLsCnwLgASajkKTSAPqM8wGXOEWtC1TqfRPH62VAKFR77CR4UrSAQqdgW392OjBGdoWd0Dmu9H6Cs4VVaqSPmvA4EHGZGew==";
	// public static final String SECUREMENT_PASSWORD_RECETTE =
	// "voq+LuDeMHsMSstg6Rlt0eWfReOz6hE1CunHUVpmD2H3sHIt6eSSgOwxrC6/KpdW4GrepHTwfKa35x3EGTVTJus2ZCSGQV4jGxWvbdSWmJAoOmN3PyVLWQk+W1OW3UnJ6AXvNW21qbSgW4QwZpU8LNylhg0yNUmMB3b6TX0YZXh8tGVIjNLa7kdMf7A83KIyNYZe3ropfWFnD76Frrc/gsSMYrgLzi5Id+6/oL5sg3Df4gn/6ERJDZigb49AIZfwNpi1NRVrh76WtE5o602RVAUg+K1TNpqvamMZgqSHqE9UTFPETL0GZkMRXXXuE0W2rpXUW8oaEN9Qy4rbdVDazg==";
	public static final String SECUREMENT_PASSWORD_INT = "iFjmcs/P0ptQr6d4TnoiQgsV3zEHYlrRfwDrQz4Yf5gUKS7TKiJnrTEAW1McmyueXdj1Jp14yQnfWceS8u5Mpb9xpODi3hdmimTwwtXQ8YPTU+cDxuHEg9swqMj8N/ZKYbRKBP8S9PlybgG/C5HuigMlXPzeaGChKgX5Qoa6AC5aheB/FesXy11VQPfghApTnXK/E2gH0jHSZJCeeJnHEyABRvcBMdGeQsTOqD6MqYXPpaHhtXjtWThTmZeipsXMZhVVM5QeISdsrQ/0j70qy9NTbtRkzAhBasWDPdRuoNuNW2tiNOgfH4I8NeZ79UhoOzN1SaOPoMPzhVZpkMGF9w==";
	// public static final String SECUREMENT_PASSWORD_PROD =
	// "240odigostw5p5pUElag4I7Bc7UkuLCDV+//HKqPhZEUEODcmJ1jNKXho294hc+pcwaxXLDPW/Rpr5Gn+C3BmrHaaUczcnRYm5v/J0NtOIsrVZ7XAjGIbgfggUDB7+he3SkSdPAT50uxGao4lRFCfm9GTYi+2yg7R3bXF6wzJWbK6pwMan5opbdjF/CZTWVXjrl+B+TyYkULTS1s36pEs4kHXC5X+EFLWsGkx0G6s0lzYp2vuAArOtjxa3kzS6r3OuUMvJQHAGbuUB0YQphYeBXn/mklgcoTGlNSmjgEQzVnULInqn1l/LBip5T6leTmseZICdGpr4kPgJGSee2mug==";
	public static final String SECUREMENT_PASSWORD_PROD = "OAlYrGsYQxo1nOWxY8GfsaJhFeigmK2AwUP9+SrTvqs9B7M2dgGPCLWmQVS8FPRvQxJVz1GopFdcncONO9Bjj1iKim1zjfjdQ43wSyl0grIIskjP2HMt/NIhgfOXl2jXxE/ZiZGWvKvjwA4jXa0BNGmpeeHIKMcSdSoKYOSlhkBcPbTlnNlajA2nCcdPFIiadthbCBgchBF22FsqSKQ3jm68SHv+88B25LeLM7bvUA4VqHCRMOjJXOzj2AZoRzc1u01DDsBJzbxt6naS9cxrLL0wq8MmaKkA5T38OhmxvJTfGR38LEobB8a+iWI/mxfvEh45rE1HZ907l7/nGGTmow==";
	public static final String SERVICE_ARCOLE_RECETTE = "https://rec.wsreseaudoc.reseau.sncf.fr:8443/recupererDocument_6/services/recupererDocument";
	public static final String SERVICE_ARCOLE_INT = "https://int.wsged.rff.ferre:8443/recupererDocument_6/services/recupererDocument";
	public static final String SERVICE_ARCOLE_PROD = "https://ws-ged.rff.ferre:8443/recupererDocument_6/services/recupererDocument";

	public static final String nomDomaineGed = "SMAJIC";
	public static final String EXLUDED_USER1 = "ADMIN";
	public static final String EXLUDED_USER2 = "RESSOURCES";
	public static final String EVENT_LOGIN = "APPLOG";
	public static final String EVENT_CHARGEMENT_COMPLET = "APPOK";
}
