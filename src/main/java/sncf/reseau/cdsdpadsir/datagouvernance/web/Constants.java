package sncf.reseau.cdsdpadsir.datagouvernance.web;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Constants implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4646697135257207095L;
	@JsonProperty("appName")
	private String appName;
	@JsonProperty("appVersion")
	private String appVersion;
	@JsonProperty("linkConnection")
	private String linkConnection;
	@JsonProperty("msgBlankPage")
	private String msgBlankPage;
	@JsonProperty("msgDeconnection")
	private String msgDeconnection;
	@JsonProperty("msgErrorPage")
	private String msgErrorPage;
	@JsonProperty("msgPageNotFound")
	private String msgPageNotFound;
	@JsonProperty("showMobileMenuOnDesktop")
	private String showMobileMenuOnDesktop;
	@JsonProperty("pageTitle.home")
	private String pageTitleHome;
	@JsonProperty("oidc.user.authorization.uri")
	private String oidcUserAuthorizationUri;
	@JsonProperty("oidc.client_id")
	private String oidcClient_id;
	@JsonProperty("oidc.redirect_uri.route")
	private String oidcRedirect_uriRoute;
	@JsonProperty("oidc.scopes")
	private String oidcScopes;
	@JsonProperty("oidc.token.name.prefix")
	private String oidcTokenNamePrefix;

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public String getLinkConnection() {
		return linkConnection;
	}

	public void setLinkConnection(String linkConnection) {
		this.linkConnection = linkConnection;
	}

	public String getMsgBlankPage() {
		return msgBlankPage;
	}

	public void setMsgBlankPage(String msgBlankPage) {
		this.msgBlankPage = msgBlankPage;
	}

	public String getMsgDeconnection() {
		return msgDeconnection;
	}

	public void setMsgDeconnection(String msgDeconnection) {
		this.msgDeconnection = msgDeconnection;
	}

	public String getMsgErrorPage() {
		return msgErrorPage;
	}

	public void setMsgErrorPage(String msgErrorPage) {
		this.msgErrorPage = msgErrorPage;
	}

	public String getMsgPageNotFound() {
		return msgPageNotFound;
	}

	public void setMsgPageNotFound(String msgPageNotFound) {
		this.msgPageNotFound = msgPageNotFound;
	}

	public String getShowMobileMenuOnDesktop() {
		return showMobileMenuOnDesktop;
	}

	public void setShowMobileMenuOnDesktop(String showMobileMenuOnDesktop) {
		this.showMobileMenuOnDesktop = showMobileMenuOnDesktop;
	}

	public String getPageTitleHome() {
		return pageTitleHome;
	}

	public void setPageTitleHome(String pageTitleHome) {
		this.pageTitleHome = pageTitleHome;
	}

	public String getOidcUserAuthorizationUri() {
		return oidcUserAuthorizationUri;
	}

	public void setOidcUserAuthorizationUri(String oidcUserAuthorizationUri) {
		this.oidcUserAuthorizationUri = oidcUserAuthorizationUri;
	}

	public String getOidcClient_id() {
		return oidcClient_id;
	}

	public void setOidcClient_id(String oidcClient_id) {
		this.oidcClient_id = oidcClient_id;
	}

	public String getOidcRedirect_uriRoute() {
		return oidcRedirect_uriRoute;
	}

	public void setOidcRedirect_uriRoute(String oidcRedirect_uriRoute) {
		this.oidcRedirect_uriRoute = oidcRedirect_uriRoute;
	}

	public String getOidcScopes() {
		return oidcScopes;
	}

	public void setOidcScopes(String oidcScopes) {
		this.oidcScopes = oidcScopes;
	}

	public String getOidcTokenNamePrefix() {
		return oidcTokenNamePrefix;
	}

	public void setOidcTokenNamePrefix(String oidcTokenNamePrefix) {
		this.oidcTokenNamePrefix = oidcTokenNamePrefix;
	}

}
