package sncf.reseau.cdsdpadsir.datagouvernance.web;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ServerJs.API + "constants")
public class ConstantsController {

	@Value("${constants.appName}")
	private String appName;
	@Value("${constants.appVersion}")
	private String appVersion;
	@Value("${constants.linkConnection}")
	private String linkConnection;
	@Value("${constants.msgBlankPage}")
	private String msgBlankPage;
	@Value("${constants.msgDeconnection}")
	private String msgDeconnection;
	@Value("${constants.msgErrorPage}")
	private String msgErrorPage;
	@Value("${constants.msgPageNotFound}")
	private String msgPageNotFound;
	private String showMobileMenuOnDesktop = "";
	@Value("${constants.pageTitle.home}")
	private String pageTitleHome;
	@Value("${constants.oidc.user.authorization.uri}")
	private String oidcUserAuthorizationUri;
	@Value("${security.oauth2.client.clientId}")
	private String oidcClient_id;
	@Value("${constants.oidc.redirect_uri.route}")
	private String oidcRedirect_uriRoute;
	@Value("${constants.oidc.scopes}")
	private String oidcScopes;
	@Value("${constants.oidc.token.name.prefix}")
	private String oidcTokenNamePrefix;

	@RequestMapping(method = RequestMethod.GET)
	public Constants getConstants() {
		Constants constants = new Constants();
		constants.setAppName(appName);
		constants.setAppVersion(appVersion);
		constants.setLinkConnection(linkConnection);
		constants.setMsgBlankPage(msgBlankPage);
		constants.setMsgDeconnection(msgDeconnection);
		constants.setMsgErrorPage(msgErrorPage);
		constants.setMsgPageNotFound(msgPageNotFound);
		constants.setOidcClient_id(oidcClient_id);
		constants.setOidcRedirect_uriRoute(oidcRedirect_uriRoute);
		constants.setOidcScopes(oidcScopes);
		constants.setOidcTokenNamePrefix(oidcTokenNamePrefix);
		constants.setOidcUserAuthorizationUri(oidcUserAuthorizationUri);
		constants.setPageTitleHome(pageTitleHome);
		constants.setShowMobileMenuOnDesktop(showMobileMenuOnDesktop);
		return constants;
	}

}
