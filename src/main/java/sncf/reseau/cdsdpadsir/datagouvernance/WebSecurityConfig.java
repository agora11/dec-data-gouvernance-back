package sncf.reseau.cdsdpadsir.datagouvernance;

import com.sncf.reseau.jraf.security.web.oidc.WebSecurityConfigOidc;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigOidc {
//public class WebSecurityConfig   {

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        // A modifier selon les droits de l’application,
        http.antMatcher("/**").authorizeRequests()
                .antMatchers("/constants").permitAll()
                .antMatchers("/v2/*").permitAll()
                .antMatchers("/autologin").permitAll();
        super.configure(http);

    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/", "/*.*", "/assets/**");
    }

}
