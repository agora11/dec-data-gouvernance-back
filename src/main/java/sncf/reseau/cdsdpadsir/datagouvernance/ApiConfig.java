package sncf.reseau.cdsdpadsir.datagouvernance;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import lombok.Getter;

/**
 * 
 * @author PNBE10831 (Nouh BOUANANE)
 *
 */
@Configuration
@Getter
public class ApiConfig {

	@Value("${info.application.name:APP}")
	private String applicationName;
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
}
